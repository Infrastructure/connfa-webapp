FROM archlinux/base
RUN pacman -Syu --noconfirm git nodejs npm tar bzip2 python2 base-devel && \
    git clone https://gitlab.gnome.org/Teams/Engagement/Events/connfa/connfa-webapp -b GUADEC
RUN cd connfa-webapp && npm install && npm run build && git rev-parse HEAD > dist/HEAD

FROM nginxinc/nginx-unprivileged:stable-alpine
COPY --from=0 /connfa-webapp/dist /var/www/connfa
ADD nginx.conf /etc/nginx/conf.d/connfa.conf
